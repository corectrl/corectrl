<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ka_GE">
<context>
    <name>AMD::PMFixedQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="17"/>
        <source>low</source>
        <translation>დაბალი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="18"/>
        <source>mid</source>
        <translation>შუა</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="19"/>
        <source>high</source>
        <translation>მაღალი</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqRangeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="20"/>
        <source>SCLK</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="21"/>
        <source>MCLK</source>
        <translation>მეხსიერება</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqVoltQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="18"/>
        <source>SCLK</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="19"/>
        <source>MCLK</source>
        <translation>მეხსიერება</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerProfileQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="16"/>
        <source>3D_FULL_SCREEN</source>
        <translation>3D სრულ ეკრანზე</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="17"/>
        <source>POWER_SAVING</source>
        <translation>ენერგიის დაზოგვა</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="18"/>
        <source>VIDEO</source>
        <translation>ვიდეო</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="19"/>
        <source>VR</source>
        <translation>VR</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="20"/>
        <source>COMPUTE</source>
        <translation>გამოთვლა</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerStateQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="17"/>
        <source>battery</source>
        <translation>ელემენტი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="18"/>
        <source>balanced</source>
        <translation>დაბალანსებული</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="19"/>
        <source>performance</source>
        <translation>წარმადობა</translation>
    </message>
</context>
<context>
    <name>AMDFanCurveForm</name>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="42"/>
        <source>Temperature</source>
        <translation>ტემპერატურა</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="43"/>
        <source>PWM</source>
        <translation>PWM</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="119"/>
        <source>Fan start</source>
        <translation>ვენტილატორის გაშვება</translation>
    </message>
</context>
<context>
    <name>AMDFanFixedForm</name>
    <message>
        <location filename="../qml/AMDFanFixedForm.qml" line="82"/>
        <source>Fan start</source>
        <translation>ვენტილატორის გაშვება</translation>
    </message>
</context>
<context>
    <name>AMDFanModeForm</name>
    <message>
        <location filename="../qml/AMDFanModeForm.qml" line="19"/>
        <source>Ventilation</source>
        <translation>ვენტილაცია</translation>
    </message>
</context>
<context>
    <name>AMDOdFanCurveForm</name>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="24"/>
        <source>Temperature</source>
        <translation>ტემპერატურა</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="25"/>
        <source>Speed</source>
        <translation>სიჩქარე</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="116"/>
        <source>Fan stop</source>
        <translation>ვენტილატორის გაჩერება</translation>
    </message>
</context>
<context>
    <name>AMDPMFixedFreqForm</name>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="75"/>
        <source>GPU</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="105"/>
        <source>Memory</source>
        <translation>მეხსიერება</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqModeForm</name>
    <message>
        <location filename="../qml/AMDPMFreqModeForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>სიხშირე</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqOdForm</name>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="46"/>
        <source>GPU</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="77"/>
        <source>Memory</source>
        <translation>მეხსიერება</translation>
    </message>
</context>
<context>
    <name>AMDPMPerfModeForm</name>
    <message>
        <location filename="../qml/AMDPMPerfModeForm.qml" line="19"/>
        <source>Performance mode</source>
        <translation>წარმადობის რეჟიმი</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerCapForm</name>
    <message>
        <location filename="../qml/AMDPMPowerCapForm.qml" line="37"/>
        <source>Power limit</source>
        <translation>კვების ზღვარი</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerProfileForm</name>
    <message>
        <location filename="../qml/AMDPMPowerProfileForm.qml" line="56"/>
        <source>Power profile</source>
        <translation>კვების პროფილი</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerStateModeForm</name>
    <message>
        <location filename="../qml/AMDPMPowerStateModeForm.qml" line="19"/>
        <source>Power management mode</source>
        <translation>კვების მართვის რეჟიმი</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltCurveForm</name>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>სიხშირე</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="21"/>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Ვოლტაჟი</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltOffsetForm</name>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Ვოლტაჟი</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="64"/>
        <source>WARNING: Operating range not available. Use with caution!</source>
        <translation>გაფრთხილება: სამუშაო შუალედი ხელმისაწვდომი არაა. ფრთხილად გამოიყენეთ!</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="93"/>
        <source>OFFSET</source>
        <translation>წანაცვლება</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="40"/>
        <source>Control your hardware with ease using application profiles</source>
        <translation>აკონტროლეთ თქვენი აპარატურა იოლად აპლიკაციის პროფილების გამოყენებით</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="45"/>
        <source>by</source>
        <translation>ავტორი</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="54"/>
        <source>Links</source>
        <translation>ბმულები</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="55"/>
        <source>Project</source>
        <translation>პროექტი</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="56"/>
        <source>Issue tracker</source>
        <translation>შეცდომების ტრეკერი</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="57"/>
        <source>Wiki</source>
        <translation>ვიკი</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="58"/>
        <source>FAQ</source>
        <translation>ხდკ</translation>
    </message>
</context>
<context>
    <name>CPUFreqForm</name>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="93"/>
        <source>Frequency governor</source>
        <translation>სიხშირის მმართველი</translation>
    </message>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="119"/>
        <source>Energy Performance Preference</source>
        <translation>ენერგიის წარმადობის არჩევანი</translation>
    </message>
</context>
<context>
    <name>CPUFreqModeForm</name>
    <message>
        <location filename="../qml/CPUFreqModeForm.qml" line="19"/>
        <source>Performance scaling</source>
        <translation>წარმადობის დამასშტაბება</translation>
    </message>
</context>
<context>
    <name>CPUFreqQMLItem</name>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="17"/>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="25"/>
        <source>performance</source>
        <translation>წარმადობა</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="18"/>
        <source>powersave</source>
        <translation>ენერგიის დაზოგვა</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="19"/>
        <source>schedutil</source>
        <translation>CPU-ის გამოყენება</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="20"/>
        <source>ondemand</source>
        <translation>საჭიროებისამებრ</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="21"/>
        <source>conservative</source>
        <translation>კონსერვატიული</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="24"/>
        <source>default</source>
        <translation>ნაგულისხმევი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="26"/>
        <source>balance_performance</source>
        <translation>წარმადობის ბალანსი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="27"/>
        <source>balance_power</source>
        <translation>კვების ბალანსი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="28"/>
        <source>power</source>
        <translation>კვება</translation>
    </message>
</context>
<context>
    <name>ControlModeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/auto/pmautoqmlitem.cpp" line="16"/>
        <source>AMD_PM_AUTO</source>
        <translation>ავტომატური</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="16"/>
        <source>AMD_PM_FIXED</source>
        <translation>მუდმივი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/auto/fanautoqmlitem.cpp" line="16"/>
        <source>AMD_FAN_AUTO</source>
        <translation>ავტომატური</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/fixed/fanfixedqmlitem.cpp" line="13"/>
        <source>AMD_FAN_FIXED</source>
        <translation>მუდმივი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/curve/fancurveqmlitem.cpp" line="19"/>
        <source>AMD_FAN_CURVE</source>
        <translation>მრუდი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/pmadvancedqmlitem.cpp" line="16"/>
        <source>AMD_PM_ADVANCED</source>
        <translation>დამატებით</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/fixedfreq/pmfixedfreqqmlitem.cpp" line="17"/>
        <source>AMD_PM_FIXED_FREQ</source>
        <translation>მუდმივი</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/dynamicfreq/pmdynamicfreqqmlitem.cpp" line="16"/>
        <source>AMD_PM_DYNAMIC_FREQ</source>
        <translation>დინამიკური</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="14"/>
        <source>CPU_CPUFREQ</source>
        <translation>მორგებული</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="16"/>
        <source>AMD_PM_POWERSTATE</source>
        <translation>მორგებული</translation>
    </message>
    <message>
        <location filename="../core/components/controls/noopqmlitem.cpp" line="16"/>
        <source>NOOP</source>
        <translation>კონტროლის გარეშე</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/auto/odfanautoqmlitem.cpp" line="16"/>
        <source>AMD_OD_FAN_AUTO</source>
        <translation>ავტომატური</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/curve/odfancurveqmlitem.cpp" line="18"/>
        <source>AMD_OD_FAN_CURVE</source>
        <translation>მრუდი</translation>
    </message>
</context>
<context>
    <name>FVControl</name>
    <message>
        <location filename="../qml/FVControl.qml" line="105"/>
        <source>STATE</source>
        <translation>მდგომარეობა</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="168"/>
        <source>Auto</source>
        <translation>ავტო</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="201"/>
        <source>Frequency</source>
        <translation>სიხშირე</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="225"/>
        <source>Voltage</source>
        <translation>Ვოლტაჟი</translation>
    </message>
</context>
<context>
    <name>FreqStateControl</name>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="61"/>
        <source>MINIMUM</source>
        <translation>მინიმუმი</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="63"/>
        <source>MAXIMUM</source>
        <translation>მაქსიმუმი</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="65"/>
        <source>STATE</source>
        <translation>მდგომარეობა</translation>
    </message>
</context>
<context>
    <name>NoopForm</name>
    <message>
        <location filename="../qml/NoopForm.qml" line="41"/>
        <source>Warning!</source>
        <translation>გაფთხილება:!</translation>
    </message>
    <message>
        <location filename="../qml/NoopForm.qml" line="46"/>
        <source>The component will not be controlled</source>
        <translation>გასაკონტროლებელი კომპონენტი</translation>
    </message>
</context>
<context>
    <name>ProfileButton</name>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Disable</source>
        <translation>გამორთვა</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Enable</source>
        <translation>ჩართვა</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="154"/>
        <source>Edit...</source>
        <translation>ჩასწორება...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="162"/>
        <source>Clone...</source>
        <translation>კლონირება...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="169"/>
        <source>Export to...</source>
        <translation>გატანა...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="178"/>
        <source>Remove</source>
        <translation>წაშლა</translation>
    </message>
</context>
<context>
    <name>ProfileInfoDialog</name>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="128"/>
        <source>Name:</source>
        <translation>სახელი:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="137"/>
        <source>Profile name</source>
        <translation>პროფილის სახელი</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="143"/>
        <source>Activation:</source>
        <translation>აქტივაცია:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="154"/>
        <source>Automatic</source>
        <translation>ავტომატური</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="155"/>
        <source>Manual</source>
        <translation>ხელით</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="166"/>
        <source>Executable:</source>
        <translation>გამსვები ფაილი:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="178"/>
        <source>Executable name</source>
        <translation>გამშვები ფაილის სახელი</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="195"/>
        <source>Select an executable file</source>
        <translation>აირჩიეთ გამშვები ფაილი</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="208"/>
        <source>Icon:</source>
        <translation>ხატულა:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="238"/>
        <source>Select an icon</source>
        <translation>აირჩიეთ ხატულა</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="239"/>
        <source>Images</source>
        <translation>გამოსახულებები</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="256"/>
        <source>Customize icon</source>
        <translation>ხატულის მორგება</translation>
    </message>
</context>
<context>
    <name>ProfileManagerUI</name>
    <message>
        <location filename="../core/profilemanagerui.cpp" line="18"/>
        <source>_global_</source>
        <translation>_გლობალური_</translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <location filename="../qml/Profiles.qml" line="150"/>
        <source>Disabled</source>
        <translation>გამორთული</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="241"/>
        <source>Profile properties</source>
        <translation>პროფილის თვისებები</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="255"/>
        <location filename="../qml/Profiles.qml" line="306"/>
        <source>New profile properties</source>
        <translation>ახალი პროფილის თვისებები</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="336"/>
        <source>Error</source>
        <translation>შეცდომა</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="357"/>
        <location filename="../qml/Profiles.qml" line="387"/>
        <location filename="../qml/Profiles.qml" line="422"/>
        <location filename="../qml/Profiles.qml" line="451"/>
        <source>Warning</source>
        <translation>გაფრთხილება</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="399"/>
        <source>Unapplied settings will be lost.
Do you want to apply them now?</source>
        <translation>გადაუტარებელი პარამეტრები დაიკარგება.
გნებავთ, ახლა გადაატაროთ ისინი?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="486"/>
        <source>Manage profiles for your applications...</source>
        <translation>პროფილების მართვა თქვენი აპლიკაციიდან...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="559"/>
        <source>Load from...</source>
        <translation>ჩატვირთვა...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="564"/>
        <source>Load settings from...</source>
        <translation>პარამეტრების ჩატვირთვა...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="570"/>
        <source>Cannot load profile.
Invalid or corrupted file.</source>
        <translation>პროფილის ჩატვირთვა შეუძლებელია.
არასწორი ან დაზიანებული ფაილი.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="434"/>
        <source>Unsaved settings will be lost.
</source>
        <translation>შეუნახავი პარამეტრები დაგეკარგებათ.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="372"/>
        <source>This action is permantent.
Do you really want to remove %1?</source>
        <translation>ეს ქმედება შეუქცევადია.
მართლა გნებავთ, წაშალოთ %1?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="435"/>
        <source>Do you want to load the default settings?</source>
        <translation>მართლა გნებავთ ნაგულისხმევი პარამეტრების ჩატვირთვა?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="463"/>
        <source>Current settings will be discarded.
</source>
        <translation>მიმდინარე პარამეტრები მოცილდება.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="464"/>
        <source>Do you want to load the saved settings?</source>
        <translation>მართლა გნებავთ შენახული პარამეტრების ჩატვირთვა?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="535"/>
        <source>Save</source>
        <translation>შენახვა</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="541"/>
        <source>Apply</source>
        <translation>გადატარება</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="547"/>
        <source>Restore</source>
        <translation>აღდგენა</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="286"/>
        <source>Export profile to...</source>
        <translation>პროფილის გატანა...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="287"/>
        <location filename="../qml/Profiles.qml" line="565"/>
        <source>CoreCtrl profile</source>
        <translation>CoreCtrl-ის პროფილი</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="295"/>
        <source>Cannot export profile.
Check the permissions of the destination file and directory.</source>
        <translation>პროფილის გატანა შეუძლებელია.
შეამოწმეთ სამიზნე ფაილის და საქაღალდის წვდომები.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="579"/>
        <source>Reset</source>
        <translation>ჩამოყრა</translation>
    </message>
</context>
<context>
    <name>SensorGraph</name>
    <message>
        <location filename="../core/components/sensors/amd/memfreqgraphitem.cpp" line="19"/>
        <source>AMD_MEM_FREQ</source>
        <translation>მეხსიერება</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpufreqgraphitem.cpp" line="19"/>
        <source>AMD_GPU_FREQ</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gputempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_TEMP</source>
        <translation>ტემპერატურა</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/powergraphitem.cpp" line="19"/>
        <source>AMD_POWER</source>
        <translation>სიმძლავრე</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/activitygraphitem.cpp" line="19"/>
        <source>AMD_ACTIVITY</source>
        <translation>აქტივობა</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memusagegraphitem.cpp" line="19"/>
        <source>AMD_MEM_USAGE</source>
        <translation>გამოყენებული მეხსიერება</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedpercgraphitem.cpp" line="19"/>
        <source>AMD_FAN_SPEED_PERC</source>
        <translation>ვენტილატორი</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedrpmgraphitem.cpp" line="21"/>
        <source>AMD_FAN_SPEED_RPM</source>
        <translation>ვენტილატორი</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpufreqpackgraphitem.cpp" line="19"/>
        <source>CPU_FREQ_PACK</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../qml/SensorGraph.qml" line="136"/>
        <source>n/a</source>
        <translation>ა/მ</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpuvoltgraphitem.cpp" line="19"/>
        <source>AMD_GPU_VOLT</source>
        <translation>ვოლტაჟი</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/junctiontempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_JUNCTION_TEMP</source>
        <translation>ტემპერატურა (დაკავშირების ადგილი)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memorytempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_MEMORY_TEMP</source>
        <translation>ტემპერატურა (მეხსიერება)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpuusagegraphitem.cpp" line="19"/>
        <source>CPU_USAGE</source>
        <translation>გამოყენება</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpucoretempgraphitem.cpp" line="19"/>
        <source>CPU_CORE_TEMP</source>
        <translation>ტემპერატურა</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="12"/>
        <source>Settings</source>
        <translation>მორგება</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>General</source>
        <translation>ზოგადი</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>Workarounds</source>
        <translation>შემოვლები</translation>
    </message>
</context>
<context>
    <name>SettingsGeneral</name>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="64"/>
        <source>Show system tray icon</source>
        <translation>სისტემის კუთხეში ხატულის ჩვენება</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="79"/>
        <source>Start minimized on system tray</source>
        <translation>სისტემურ საათთან ჩაკეცილი გაშვება</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="93"/>
        <source>Save window geometry</source>
        <translation>ფანჯრის გეომეტრიის შენახვა</translation>
    </message>
</context>
<context>
    <name>SettingsWorkarounds</name>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="112"/>
        <source>Sensors</source>
        <translation>სენსორები</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="123"/>
        <source>Disabled sensors won&apos;t be updated from hardware</source>
        <translation>გათიშული სენსორები არ განახლდება აპარატურიდან</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="133"/>
        <source>Device</source>
        <translation>მოწყობილობა</translation>
    </message>
</context>
<context>
    <name>SysTray</name>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Hide</source>
        <translation>დამალვა</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Show</source>
        <translation>ჩვენება</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="164"/>
        <source>Manual profiles</source>
        <translation>მომხმარებლის პროფილები</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="168"/>
        <source>Quit</source>
        <translation>გასვლა</translation>
    </message>
</context>
<context>
    <name>System</name>
    <message>
        <location filename="../qml/System.qml" line="88"/>
        <source>Information and application settings...</source>
        <translation>ინფორმაცია და აპლიკაციის მორგება...</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="98"/>
        <source>Settings</source>
        <translation>მორგება</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="104"/>
        <source>Copy all</source>
        <translation>ყველას კოპირება</translation>
    </message>
</context>
<context>
    <name>SystemInfoUI</name>
    <message>
        <location filename="../core/systeminfoui.cpp" line="19"/>
        <source>kernelv</source>
        <translation>ბირთვის ვერსია</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="20"/>
        <source>mesav</source>
        <translation>mesa-ის ვერსია</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="21"/>
        <source>vkapiv</source>
        <translation>vk API-ის ვერსია</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="22"/>
        <source>glcorev</source>
        <translation>gl-ის ბირთვის ვერსია</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="23"/>
        <source>glcompv</source>
        <translation>OpenGL-ის ვერსია (თავსებადობა)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="24"/>
        <source>vendorid</source>
        <translation>მომწოდებლის ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="25"/>
        <source>deviceid</source>
        <translation>მოწყობილობის ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="26"/>
        <source>svendorid</source>
        <translation>მომწოდებლის მოდელის ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="27"/>
        <source>sdeviceid</source>
        <translation>მოწყობილობის მოდელის ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="28"/>
        <source>vendor</source>
        <translation>მომწოდებელი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="29"/>
        <source>device</source>
        <translation>მოწყობილობა</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="30"/>
        <source>sdevice</source>
        <translation>მოწყობილობის მოდელი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="31"/>
        <source>pcislot</source>
        <translation>pci სლოტი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="32"/>
        <source>driver</source>
        <translation>დრაივერი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="33"/>
        <source>revision</source>
        <translation>რევიზია</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="34"/>
        <source>memory</source>
        <translation>მეხსიერება</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="35"/>
        <source>gputype</source>
        <translation>gpu-ის ტიპი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="36"/>
        <source>biosv</source>
        <translation>bios-ის მოდელი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="37"/>
        <source>cpufamily</source>
        <translation>პროცესორის ოჯახი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="38"/>
        <source>model</source>
        <translation>მოდელი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="39"/>
        <source>modname</source>
        <translation>მოდელის სახელი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="40"/>
        <source>stepping</source>
        <translation>ბიჯი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="41"/>
        <source>ucodev</source>
        <translation>მიკროკოდის ვერსია</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="42"/>
        <source>l3cache</source>
        <translation>l3 კეში</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="44"/>
        <source>cores</source>
        <translation>ბირთვები</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="45"/>
        <source>flags</source>
        <translation>ალმები</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="46"/>
        <source>bugs</source>
        <translation>შეცდომები</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="47"/>
        <source>bogomips</source>
        <translation>Bogomips</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="48"/>
        <source>arch</source>
        <translation>arch</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="49"/>
        <source>opmode</source>
        <translation>ოპერაციის რეჟიმი</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="50"/>
        <source>byteorder</source>
        <translation>ბაიტების მიმდევრობა</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="51"/>
        <source>virt</source>
        <translation>ვირტ</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="52"/>
        <source>l1dcache</source>
        <translation>l1d კეში</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="53"/>
        <source>l1icache</source>
        <translation>l1i კეში</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="54"/>
        <source>l2cache</source>
        <translation>l2 კეში</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="55"/>
        <source>uniqueid</source>
        <translation>უნიკალურიid</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="43"/>
        <source>exeunits</source>
        <translation>შემსრულებელი ერთეულები</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="43"/>
        <source>Profiles</source>
        <translation>პროფილები</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="46"/>
        <source>System</source>
        <translation>სისტემა</translation>
    </message>
</context>
</TS>
