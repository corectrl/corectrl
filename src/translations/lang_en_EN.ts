<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AMD::PMFixedQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="17"/>
        <source>low</source>
        <translation>Low</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="18"/>
        <source>mid</source>
        <translation>Mid</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="19"/>
        <source>high</source>
        <translation>High</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqRangeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="20"/>
        <source>SCLK</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="21"/>
        <source>MCLK</source>
        <translation>Memory</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqVoltQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="18"/>
        <source>SCLK</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="19"/>
        <source>MCLK</source>
        <translation>Memory</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerProfileQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="16"/>
        <source>3D_FULL_SCREEN</source>
        <translation>3D Fullscreen</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="17"/>
        <source>POWER_SAVING</source>
        <translation>Power saving</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="18"/>
        <source>VIDEO</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="19"/>
        <source>VR</source>
        <translation>Virtual reality</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="20"/>
        <source>COMPUTE</source>
        <translation>Compute</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerStateQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="17"/>
        <source>battery</source>
        <translation>Battery</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="18"/>
        <source>balanced</source>
        <translation>Balanced</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="19"/>
        <source>performance</source>
        <translation>Performance</translation>
    </message>
</context>
<context>
    <name>AMDFanCurveForm</name>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="42"/>
        <source>Temperature</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="43"/>
        <source>PWM</source>
        <translation>PWM</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="119"/>
        <source>Fan start</source>
        <translation>Fan start</translation>
    </message>
</context>
<context>
    <name>AMDFanFixedForm</name>
    <message>
        <location filename="../qml/AMDFanFixedForm.qml" line="82"/>
        <source>Fan start</source>
        <translation>Fan start</translation>
    </message>
</context>
<context>
    <name>AMDFanModeForm</name>
    <message>
        <location filename="../qml/AMDFanModeForm.qml" line="19"/>
        <source>Ventilation</source>
        <translation>Ventilation</translation>
    </message>
</context>
<context>
    <name>AMDOdFanCurveForm</name>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="24"/>
        <source>Temperature</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="25"/>
        <source>Speed</source>
        <translation>Speed</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="116"/>
        <source>Fan stop</source>
        <translation>Fan stop</translation>
    </message>
</context>
<context>
    <name>AMDPMFixedFreqForm</name>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="75"/>
        <source>GPU</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="105"/>
        <source>Memory</source>
        <translation>Memory</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqModeForm</name>
    <message>
        <location filename="../qml/AMDPMFreqModeForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqOdForm</name>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="46"/>
        <source>GPU</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="77"/>
        <source>Memory</source>
        <translation>Memory</translation>
    </message>
</context>
<context>
    <name>AMDPMPerfModeForm</name>
    <message>
        <location filename="../qml/AMDPMPerfModeForm.qml" line="19"/>
        <source>Performance mode</source>
        <translation>Performance mode</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerCapForm</name>
    <message>
        <location filename="../qml/AMDPMPowerCapForm.qml" line="37"/>
        <source>Power limit</source>
        <translation>Power limit</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerProfileForm</name>
    <message>
        <location filename="../qml/AMDPMPowerProfileForm.qml" line="56"/>
        <source>Power profile</source>
        <translation>Power profile</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerStateModeForm</name>
    <message>
        <location filename="../qml/AMDPMPowerStateModeForm.qml" line="19"/>
        <source>Power management mode</source>
        <translation>Power management mode</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltCurveForm</name>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="21"/>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Voltage</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltOffsetForm</name>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Voltage</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="64"/>
        <source>WARNING: Operating range not available. Use with caution!</source>
        <translation>WARNING: Operating range not available. Use with caution!</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="93"/>
        <source>OFFSET</source>
        <translation>OFFSET</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="40"/>
        <source>Control your hardware with ease using application profiles</source>
        <translation>Control your hardware with ease using application profiles</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="45"/>
        <source>by</source>
        <translation>by</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="54"/>
        <source>Links</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="55"/>
        <source>Project</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="56"/>
        <source>Issue tracker</source>
        <translation>Issue tracker</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="57"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="58"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
</context>
<context>
    <name>CPUFreqForm</name>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="93"/>
        <source>Frequency governor</source>
        <translation>Frequency governor</translation>
    </message>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="119"/>
        <source>Energy Performance Preference</source>
        <translation>Energy Performance Preference</translation>
    </message>
</context>
<context>
    <name>CPUFreqModeForm</name>
    <message>
        <location filename="../qml/CPUFreqModeForm.qml" line="19"/>
        <source>Performance scaling</source>
        <translation>Performance scaling</translation>
    </message>
</context>
<context>
    <name>CPUFreqQMLItem</name>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="17"/>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="25"/>
        <source>performance</source>
        <translation>Performance</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="18"/>
        <source>powersave</source>
        <translation>Powersave</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="19"/>
        <source>schedutil</source>
        <translation>CPU utilization</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="20"/>
        <source>ondemand</source>
        <translation>Ondemand</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="21"/>
        <source>conservative</source>
        <translation>Conservative</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="24"/>
        <source>default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="26"/>
        <source>balance_performance</source>
        <translation>Balance performance</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="27"/>
        <source>balance_power</source>
        <translation>Balance power</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="28"/>
        <source>power</source>
        <translation>Power</translation>
    </message>
</context>
<context>
    <name>ControlModeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/auto/pmautoqmlitem.cpp" line="16"/>
        <source>AMD_PM_AUTO</source>
        <translation>Automatic</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="16"/>
        <source>AMD_PM_FIXED</source>
        <translation>Fixed</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/auto/fanautoqmlitem.cpp" line="16"/>
        <source>AMD_FAN_AUTO</source>
        <translation>Automatic</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/fixed/fanfixedqmlitem.cpp" line="13"/>
        <source>AMD_FAN_FIXED</source>
        <translation>Fixed</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/curve/fancurveqmlitem.cpp" line="19"/>
        <source>AMD_FAN_CURVE</source>
        <translation>Curve</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/pmadvancedqmlitem.cpp" line="16"/>
        <source>AMD_PM_ADVANCED</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/fixedfreq/pmfixedfreqqmlitem.cpp" line="17"/>
        <source>AMD_PM_FIXED_FREQ</source>
        <translation>Fixed</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/dynamicfreq/pmdynamicfreqqmlitem.cpp" line="16"/>
        <source>AMD_PM_DYNAMIC_FREQ</source>
        <translation>Dynamic</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="14"/>
        <source>CPU_CPUFREQ</source>
        <translation>Custom</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="16"/>
        <source>AMD_PM_POWERSTATE</source>
        <translation>Custom</translation>
    </message>
    <message>
        <location filename="../core/components/controls/noopqmlitem.cpp" line="16"/>
        <source>NOOP</source>
        <translation>Do not control</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/auto/odfanautoqmlitem.cpp" line="16"/>
        <source>AMD_OD_FAN_AUTO</source>
        <translation>Automatic</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/curve/odfancurveqmlitem.cpp" line="18"/>
        <source>AMD_OD_FAN_CURVE</source>
        <translation>Curve</translation>
    </message>
</context>
<context>
    <name>FVControl</name>
    <message>
        <location filename="../qml/FVControl.qml" line="105"/>
        <source>STATE</source>
        <translation>STATE</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="168"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="201"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="225"/>
        <source>Voltage</source>
        <translation>Voltage</translation>
    </message>
</context>
<context>
    <name>FreqStateControl</name>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="61"/>
        <source>MINIMUM</source>
        <translation>MINIMUM</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="63"/>
        <source>MAXIMUM</source>
        <translation>MAXIMUM</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="65"/>
        <source>STATE</source>
        <translation>STATE</translation>
    </message>
</context>
<context>
    <name>NoopForm</name>
    <message>
        <location filename="../qml/NoopForm.qml" line="41"/>
        <source>Warning!</source>
        <translation>Warning!</translation>
    </message>
    <message>
        <location filename="../qml/NoopForm.qml" line="46"/>
        <source>The component will not be controlled</source>
        <translation>The component will not be controlled</translation>
    </message>
</context>
<context>
    <name>ProfileButton</name>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Disable</source>
        <translation>Disable</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Enable</source>
        <translation>Enable</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="154"/>
        <source>Edit...</source>
        <translation>Edit...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="162"/>
        <source>Clone...</source>
        <translation>Clone...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="169"/>
        <source>Export to...</source>
        <translation>Export to...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="178"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>ProfileInfoDialog</name>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="128"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="137"/>
        <source>Profile name</source>
        <translation>Profile name</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="143"/>
        <source>Activation:</source>
        <translation>Activation:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="154"/>
        <source>Automatic</source>
        <translation>Automatic</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="155"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="166"/>
        <source>Executable:</source>
        <translation>Executable:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="178"/>
        <source>Executable name</source>
        <translation>Executable name</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="195"/>
        <source>Select an executable file</source>
        <translation>Select an executable file</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="208"/>
        <source>Icon:</source>
        <translation>Icon:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="238"/>
        <source>Select an icon</source>
        <translation>Select an icon</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="239"/>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="256"/>
        <source>Customize icon</source>
        <translation>Customize icon</translation>
    </message>
</context>
<context>
    <name>ProfileManagerUI</name>
    <message>
        <location filename="../core/profilemanagerui.cpp" line="18"/>
        <source>_global_</source>
        <translation>Global Profile</translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <location filename="../qml/Profiles.qml" line="150"/>
        <source>Disabled</source>
        <translation>Disabled</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="241"/>
        <source>Profile properties</source>
        <translation>Profile properties</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="255"/>
        <location filename="../qml/Profiles.qml" line="306"/>
        <source>New profile properties</source>
        <translation>New profile properties</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="336"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="357"/>
        <location filename="../qml/Profiles.qml" line="387"/>
        <location filename="../qml/Profiles.qml" line="422"/>
        <location filename="../qml/Profiles.qml" line="451"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="399"/>
        <source>Unapplied settings will be lost.
Do you want to apply them now?</source>
        <translation>Unapplied settings will be lost.
Do you want to apply them now?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="486"/>
        <source>Manage profiles for your applications...</source>
        <translation>Manage profiles for your applications...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="559"/>
        <source>Load from...</source>
        <translation>Load from...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="564"/>
        <source>Load settings from...</source>
        <translation>Load settings from...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="570"/>
        <source>Cannot load profile.
Invalid or corrupted file.</source>
        <translation>Cannot load profile.
Invalid or corrupted file.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="434"/>
        <source>Unsaved settings will be lost.
</source>
        <translation>Unsaved settings will be lost.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="372"/>
        <source>This action is permantent.
Do you really want to remove %1?</source>
        <translation>This action is permantent.
Do you really want to remove %1?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="435"/>
        <source>Do you want to load the default settings?</source>
        <translation>Do you want to load the default settings?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="463"/>
        <source>Current settings will be discarded.
</source>
        <translation>Current settings will be discarded.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="464"/>
        <source>Do you want to load the saved settings?</source>
        <translation>Do you want to load the saved settings?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="535"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="541"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="547"/>
        <source>Restore</source>
        <translation>Restore</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="286"/>
        <source>Export profile to...</source>
        <translation>Export profile to...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="287"/>
        <location filename="../qml/Profiles.qml" line="565"/>
        <source>CoreCtrl profile</source>
        <translation>CoreCtrl profile</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="295"/>
        <source>Cannot export profile.
Check the permissions of the destination file and directory.</source>
        <translation>Cannot export profile.
Check the permissions of the destination file and directory.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="579"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
</context>
<context>
    <name>SensorGraph</name>
    <message>
        <location filename="../core/components/sensors/amd/memfreqgraphitem.cpp" line="19"/>
        <source>AMD_MEM_FREQ</source>
        <translation>Memory</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpufreqgraphitem.cpp" line="19"/>
        <source>AMD_GPU_FREQ</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gputempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_TEMP</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/powergraphitem.cpp" line="19"/>
        <source>AMD_POWER</source>
        <translation>Power</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/activitygraphitem.cpp" line="19"/>
        <source>AMD_ACTIVITY</source>
        <translation>Activity</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memusagegraphitem.cpp" line="19"/>
        <source>AMD_MEM_USAGE</source>
        <translation>Used memory</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedpercgraphitem.cpp" line="19"/>
        <source>AMD_FAN_SPEED_PERC</source>
        <translation>Fan</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedrpmgraphitem.cpp" line="21"/>
        <source>AMD_FAN_SPEED_RPM</source>
        <translation>Fan</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpufreqpackgraphitem.cpp" line="19"/>
        <source>CPU_FREQ_PACK</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../qml/SensorGraph.qml" line="136"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpuvoltgraphitem.cpp" line="19"/>
        <source>AMD_GPU_VOLT</source>
        <translation>Voltage</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/junctiontempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_JUNCTION_TEMP</source>
        <translation>Temperature (junction)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memorytempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_MEMORY_TEMP</source>
        <translation>Temperature (memory)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpuusagegraphitem.cpp" line="19"/>
        <source>CPU_USAGE</source>
        <translation>Usage</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpucoretempgraphitem.cpp" line="19"/>
        <source>CPU_CORE_TEMP</source>
        <translation>Temperature</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="12"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>Workarounds</source>
        <translation>Workarounds</translation>
    </message>
</context>
<context>
    <name>SettingsGeneral</name>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="64"/>
        <source>Show system tray icon</source>
        <translation>Show system tray icon</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="79"/>
        <source>Start minimized on system tray</source>
        <translation>Start minimized on system tray</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="93"/>
        <source>Save window geometry</source>
        <translation>Save window geometry</translation>
    </message>
</context>
<context>
    <name>SettingsWorkarounds</name>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="112"/>
        <source>Sensors</source>
        <translation>Sensors</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="123"/>
        <source>Disabled sensors won&apos;t be updated from hardware</source>
        <translation>Disabled sensors won&apos;t be updated from hardware</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="133"/>
        <source>Device</source>
        <translation>Device</translation>
    </message>
</context>
<context>
    <name>SysTray</name>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Hide</source>
        <translation>Hide</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Show</source>
        <translation>Show</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="164"/>
        <source>Manual profiles</source>
        <translation>Manual profiles</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="168"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
</context>
<context>
    <name>System</name>
    <message>
        <location filename="../qml/System.qml" line="88"/>
        <source>Information and application settings...</source>
        <translation>Information and application settings...</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="98"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="104"/>
        <source>Copy all</source>
        <translation>Copy all</translation>
    </message>
</context>
<context>
    <name>SystemInfoUI</name>
    <message>
        <location filename="../core/systeminfoui.cpp" line="19"/>
        <source>kernelv</source>
        <translation>Kernel version</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="20"/>
        <source>mesav</source>
        <translation>Mesa version</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="21"/>
        <source>vkapiv</source>
        <translation>Vulkan API version</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="22"/>
        <source>glcorev</source>
        <translation>OpenGL version (core)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="23"/>
        <source>glcompv</source>
        <translation>OpenGL version (compat)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="24"/>
        <source>vendorid</source>
        <translation>Vendor ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="25"/>
        <source>deviceid</source>
        <translation>Device ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="26"/>
        <source>svendorid</source>
        <translation>Vendor model ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="27"/>
        <source>sdeviceid</source>
        <translation>Device model ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="28"/>
        <source>vendor</source>
        <translation>Vendor</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="29"/>
        <source>device</source>
        <translation>Device</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="30"/>
        <source>sdevice</source>
        <translation>Device model</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="31"/>
        <source>pcislot</source>
        <translation>PCI Slot</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="32"/>
        <source>driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="33"/>
        <source>revision</source>
        <translation>Revision</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="34"/>
        <source>memory</source>
        <translation>Memory</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="35"/>
        <source>gputype</source>
        <translation>GPU Type</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="36"/>
        <source>biosv</source>
        <translation>BIOS version</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="37"/>
        <source>cpufamily</source>
        <translation>CPU Family</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="38"/>
        <source>model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="39"/>
        <source>modname</source>
        <translation>Model Name</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="40"/>
        <source>stepping</source>
        <translation>Stepping</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="41"/>
        <source>ucodev</source>
        <translation>Microcode version</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="42"/>
        <source>l3cache</source>
        <translation>L3 cache</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="44"/>
        <source>cores</source>
        <translation>Cores</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="45"/>
        <source>flags</source>
        <translation>Flags</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="46"/>
        <source>bugs</source>
        <translation>Bugs</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="47"/>
        <source>bogomips</source>
        <translation>Bogomips</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="48"/>
        <source>arch</source>
        <translation>Architecture</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="49"/>
        <source>opmode</source>
        <translation>Operation modes</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="50"/>
        <source>byteorder</source>
        <translation>Byte order</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="51"/>
        <source>virt</source>
        <translation>Virtualization</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="52"/>
        <source>l1dcache</source>
        <translation>L1 cache (data)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="53"/>
        <source>l1icache</source>
        <translation>L1 cache (instructions)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="54"/>
        <source>l2cache</source>
        <translation>L2 cache</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="55"/>
        <source>uniqueid</source>
        <translation>Unique ID</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="43"/>
        <source>exeunits</source>
        <translation>Execution units</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="43"/>
        <source>Profiles</source>
        <translation>Profiles</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="46"/>
        <source>System</source>
        <translation>System</translation>
    </message>
</context>
</TS>
