<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>AMD::PMFixedQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="17"/>
        <source>low</source>
        <translation>Baix</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="18"/>
        <source>mid</source>
        <translation>Mig</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="19"/>
        <source>high</source>
        <translation>Alt</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqRangeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="20"/>
        <source>SCLK</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="21"/>
        <source>MCLK</source>
        <translation>Memòria</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqVoltQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="18"/>
        <source>SCLK</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="19"/>
        <source>MCLK</source>
        <translation>Memòria</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerProfileQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="16"/>
        <source>3D_FULL_SCREEN</source>
        <translation>3D a pantalla completa</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="17"/>
        <source>POWER_SAVING</source>
        <translation>Estalvi d&apos;energia</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="18"/>
        <source>VIDEO</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="19"/>
        <source>VR</source>
        <translation>Realitat virtual</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="20"/>
        <source>COMPUTE</source>
        <translation>Computació</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerStateQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="17"/>
        <source>battery</source>
        <translation>Bateria</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="18"/>
        <source>balanced</source>
        <translation>Balancejat</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="19"/>
        <source>performance</source>
        <translation>Rendiment</translation>
    </message>
</context>
<context>
    <name>AMDFanCurveForm</name>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="42"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="43"/>
        <source>PWM</source>
        <translation>PWM</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="119"/>
        <source>Fan start</source>
        <translation>Inici del ventilador</translation>
    </message>
</context>
<context>
    <name>AMDFanFixedForm</name>
    <message>
        <location filename="../qml/AMDFanFixedForm.qml" line="82"/>
        <source>Fan start</source>
        <translation>Inici del ventilador</translation>
    </message>
</context>
<context>
    <name>AMDFanModeForm</name>
    <message>
        <location filename="../qml/AMDFanModeForm.qml" line="19"/>
        <source>Ventilation</source>
        <translation>Ventilació</translation>
    </message>
</context>
<context>
    <name>AMDOdFanCurveForm</name>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="24"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="25"/>
        <source>Speed</source>
        <translation>Velocitat</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="116"/>
        <source>Fan stop</source>
        <translation>Parada de ventilador</translation>
    </message>
</context>
<context>
    <name>AMDPMFixedFreqForm</name>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="75"/>
        <source>GPU</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="105"/>
        <source>Memory</source>
        <translation>Memòria</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqModeForm</name>
    <message>
        <location filename="../qml/AMDPMFreqModeForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>Frequència</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqOdForm</name>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="46"/>
        <source>GPU</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="77"/>
        <source>Memory</source>
        <translation>Memòria</translation>
    </message>
</context>
<context>
    <name>AMDPMPerfModeForm</name>
    <message>
        <location filename="../qml/AMDPMPerfModeForm.qml" line="19"/>
        <source>Performance mode</source>
        <translation>Mode de rendiment</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerCapForm</name>
    <message>
        <location filename="../qml/AMDPMPowerCapForm.qml" line="37"/>
        <source>Power limit</source>
        <translation>Limit d&apos;energia</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerProfileForm</name>
    <message>
        <location filename="../qml/AMDPMPowerProfileForm.qml" line="56"/>
        <source>Power profile</source>
        <translation>Perfil d&apos;energia</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerStateModeForm</name>
    <message>
        <location filename="../qml/AMDPMPowerStateModeForm.qml" line="19"/>
        <source>Power management mode</source>
        <translation>Mode de gestió d&apos;energia</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltCurveForm</name>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>Frequència</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="21"/>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Voltatge</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltOffsetForm</name>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Voltatge</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="64"/>
        <source>WARNING: Operating range not available. Use with caution!</source>
        <translation>AVÍS: El rang de funcionament no està disponible. Utilitzeu-lo amb precaució!</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="93"/>
        <source>OFFSET</source>
        <translation>COMPENSACIÓ</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="40"/>
        <source>Control your hardware with ease using application profiles</source>
        <translation>Controla el teu maquinari amb facilitat utilitzant perfils d&apos;aplicacions</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="45"/>
        <source>by</source>
        <translation>per</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="54"/>
        <source>Links</source>
        <translation>Enllaços</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="55"/>
        <source>Project</source>
        <translation>Projecte</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="56"/>
        <source>Issue tracker</source>
        <translation>Seguimient de problemes</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="57"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="58"/>
        <source>FAQ</source>
        <translation>Preguntes freqüents</translation>
    </message>
</context>
<context>
    <name>CPUFreqForm</name>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="93"/>
        <source>Frequency governor</source>
        <translation>Gobernador de frequència</translation>
    </message>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="119"/>
        <source>Energy Performance Preference</source>
        <translation>Preferència de rendiment energètic</translation>
    </message>
</context>
<context>
    <name>CPUFreqModeForm</name>
    <message>
        <location filename="../qml/CPUFreqModeForm.qml" line="19"/>
        <source>Performance scaling</source>
        <translation>Escalat de rendiment</translation>
    </message>
</context>
<context>
    <name>CPUFreqQMLItem</name>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="17"/>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="25"/>
        <source>performance</source>
        <translation>Rendiment</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="18"/>
        <source>powersave</source>
        <translation>Estalvi d&apos;energia</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="19"/>
        <source>schedutil</source>
        <translation>Utilització de la CPU</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="20"/>
        <source>ondemand</source>
        <translation>Sota demanda</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="21"/>
        <source>conservative</source>
        <translation>Conservador</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="24"/>
        <source>default</source>
        <translation>Per defecte</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="26"/>
        <source>balance_performance</source>
        <translation>Equilibrar rendiment</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="27"/>
        <source>balance_power</source>
        <translation>Equilibrar energia</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="28"/>
        <source>power</source>
        <translation>Energia</translation>
    </message>
</context>
<context>
    <name>ControlModeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/auto/pmautoqmlitem.cpp" line="16"/>
        <source>AMD_PM_AUTO</source>
        <translation>Automàtic</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="16"/>
        <source>AMD_PM_FIXED</source>
        <translation>Fixe</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/auto/fanautoqmlitem.cpp" line="16"/>
        <source>AMD_FAN_AUTO</source>
        <translation>Automàtic</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/fixed/fanfixedqmlitem.cpp" line="13"/>
        <source>AMD_FAN_FIXED</source>
        <translation>Fixe</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/curve/fancurveqmlitem.cpp" line="19"/>
        <source>AMD_FAN_CURVE</source>
        <translation>Corba</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/pmadvancedqmlitem.cpp" line="16"/>
        <source>AMD_PM_ADVANCED</source>
        <translation>Avançat</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/fixedfreq/pmfixedfreqqmlitem.cpp" line="17"/>
        <source>AMD_PM_FIXED_FREQ</source>
        <translation>Fixe</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/dynamicfreq/pmdynamicfreqqmlitem.cpp" line="16"/>
        <source>AMD_PM_DYNAMIC_FREQ</source>
        <translation>Dinàmica</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="14"/>
        <source>CPU_CPUFREQ</source>
        <translation>Personalitzat</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="16"/>
        <source>AMD_PM_POWERSTATE</source>
        <translation>Personalitzat</translation>
    </message>
    <message>
        <location filename="../core/components/controls/noopqmlitem.cpp" line="16"/>
        <source>NOOP</source>
        <translation>No controlar</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/auto/odfanautoqmlitem.cpp" line="16"/>
        <source>AMD_OD_FAN_AUTO</source>
        <translation>Automàtic</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/curve/odfancurveqmlitem.cpp" line="18"/>
        <source>AMD_OD_FAN_CURVE</source>
        <translation>Corba</translation>
    </message>
</context>
<context>
    <name>FVControl</name>
    <message>
        <location filename="../qml/FVControl.qml" line="105"/>
        <source>STATE</source>
        <translation>ESTAT</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="168"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="201"/>
        <source>Frequency</source>
        <translation>Frequència</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="225"/>
        <source>Voltage</source>
        <translation>Voltatge</translation>
    </message>
</context>
<context>
    <name>FreqStateControl</name>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="61"/>
        <source>MINIMUM</source>
        <translation>MÍNIMA</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="63"/>
        <source>MAXIMUM</source>
        <translation>MÁXIMA</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="65"/>
        <source>STATE</source>
        <translation>ESTAT</translation>
    </message>
</context>
<context>
    <name>NoopForm</name>
    <message>
        <location filename="../qml/NoopForm.qml" line="41"/>
        <source>Warning!</source>
        <translation>¡Advertència!</translation>
    </message>
    <message>
        <location filename="../qml/NoopForm.qml" line="46"/>
        <source>The component will not be controlled</source>
        <translation>El component no es controlarà</translation>
    </message>
</context>
<context>
    <name>ProfileButton</name>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Disable</source>
        <translation>Deshabilitar</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Enable</source>
        <translation>Habilitar</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="154"/>
        <source>Edit...</source>
        <translation>Editar...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="162"/>
        <source>Clone...</source>
        <translation>Duplicar...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="169"/>
        <source>Export to...</source>
        <translation>Exportar a...</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="178"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>ProfileInfoDialog</name>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="128"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="137"/>
        <source>Profile name</source>
        <translation>Nom del perfil</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="143"/>
        <source>Activation:</source>
        <translation>Activació:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="154"/>
        <source>Automatic</source>
        <translation>Automàtic</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="155"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="166"/>
        <source>Executable:</source>
        <translation>Executable:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="178"/>
        <source>Executable name</source>
        <translation>Nom del executable</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="195"/>
        <source>Select an executable file</source>
        <translation>Seleciona un fitxer executable</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="208"/>
        <source>Icon:</source>
        <translation>Icona:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="238"/>
        <source>Select an icon</source>
        <translation>Selecciona una icona</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="239"/>
        <source>Images</source>
        <translation>Imatges</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="256"/>
        <source>Customize icon</source>
        <translation>Personalitza la icona</translation>
    </message>
</context>
<context>
    <name>ProfileManagerUI</name>
    <message>
        <location filename="../core/profilemanagerui.cpp" line="18"/>
        <source>_global_</source>
        <translation>Perfil Global</translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <location filename="../qml/Profiles.qml" line="150"/>
        <source>Disabled</source>
        <translation>Deshabilitat</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="241"/>
        <source>Profile properties</source>
        <translation>Propietats del perfil</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="255"/>
        <location filename="../qml/Profiles.qml" line="306"/>
        <source>New profile properties</source>
        <translation>Propietats del nou perfil</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="336"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="357"/>
        <location filename="../qml/Profiles.qml" line="387"/>
        <location filename="../qml/Profiles.qml" line="422"/>
        <location filename="../qml/Profiles.qml" line="451"/>
        <source>Warning</source>
        <translation>Atenció</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="372"/>
        <source>This action is permantent.
Do you really want to remove %1?</source>
        <translation>Aquesta acció és permanent.
        Segur que vols eliminar %1?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="399"/>
        <source>Unapplied settings will be lost.
Do you want to apply them now?</source>
        <translation>Els paràmetres sense aplicar es perderán.
Vols aplicar-los ara?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="486"/>
        <source>Manage profiles for your applications...</source>
        <translation>Gestiona perfils per a les teves aplicacions...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="559"/>
        <source>Load from...</source>
        <translation>Carregar des de ...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="564"/>
        <source>Load settings from...</source>
        <translation>Carregar paràmetres des de ...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="570"/>
        <source>Cannot load profile.
Invalid or corrupted file.</source>
        <translation>No es pot carregar el perfil.
Arxiu erròni o corrupte.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="434"/>
        <source>Unsaved settings will be lost.
</source>
        <translation>Els paràmetres sense guardar es perderán.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="435"/>
        <source>Do you want to load the default settings?</source>
        <translation>Vols carregar els paràmetres per defecte?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="463"/>
        <source>Current settings will be discarded.
</source>
        <translation>Els paràmetres actuals serán descartats.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="464"/>
        <source>Do you want to load the saved settings?</source>
        <translation>Vols carregar els paràmetres guardats?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="535"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="541"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="547"/>
        <source>Restore</source>
        <translation>Restaurar</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="286"/>
        <source>Export profile to...</source>
        <translation>Exportar perfil a...</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="287"/>
        <location filename="../qml/Profiles.qml" line="565"/>
        <source>CoreCtrl profile</source>
        <translation>Perfil de CoreCtrl</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="295"/>
        <source>Cannot export profile.
Check the permissions of the destination file and directory.</source>
        <translation>El perfil no es pot exportar.
Comprova els permisos del fitxer i del directori de destí.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="579"/>
        <source>Reset</source>
        <translation>Reinicialitza</translation>
    </message>
</context>
<context>
    <name>SensorGraph</name>
    <message>
        <location filename="../core/components/sensors/amd/memfreqgraphitem.cpp" line="19"/>
        <source>AMD_MEM_FREQ</source>
        <translation>Memòria</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpufreqgraphitem.cpp" line="19"/>
        <source>AMD_GPU_FREQ</source>
        <translation>GPU</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gputempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_TEMP</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/powergraphitem.cpp" line="19"/>
        <source>AMD_POWER</source>
        <translation>Consum</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/activitygraphitem.cpp" line="19"/>
        <source>AMD_ACTIVITY</source>
        <translation>Activitat</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memusagegraphitem.cpp" line="19"/>
        <source>AMD_MEM_USAGE</source>
        <translation>Memòria usada</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedpercgraphitem.cpp" line="19"/>
        <source>AMD_FAN_SPEED_PERC</source>
        <translation>Ventilador</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedrpmgraphitem.cpp" line="21"/>
        <source>AMD_FAN_SPEED_RPM</source>
        <translation>Ventilador</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpufreqpackgraphitem.cpp" line="19"/>
        <source>CPU_FREQ_PACK</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../qml/SensorGraph.qml" line="136"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpuvoltgraphitem.cpp" line="19"/>
        <source>AMD_GPU_VOLT</source>
        <translation>Voltatge</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/junctiontempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_JUNCTION_TEMP</source>
        <translation>Temperatura (unió)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memorytempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_MEMORY_TEMP</source>
        <translation>Temperatura (memòria)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpuusagegraphitem.cpp" line="19"/>
        <source>CPU_USAGE</source>
        <translation>Ús</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpucoretempgraphitem.cpp" line="19"/>
        <source>CPU_CORE_TEMP</source>
        <translation>Temperatura</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="12"/>
        <source>Settings</source>
        <translation>Paràmetres</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>Workarounds</source>
        <translation>Resolucions</translation>
    </message>
</context>
<context>
    <name>SettingsGeneral</name>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="64"/>
        <source>Show system tray icon</source>
        <translation>Mostrar icona a la safata del sistema</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="79"/>
        <source>Start minimized on system tray</source>
        <translation>Iniciar minimitzat a la safata del sistema</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="93"/>
        <source>Save window geometry</source>
        <translation>Desar la geometria de la finestra</translation>
    </message>
</context>
<context>
    <name>SettingsWorkarounds</name>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="112"/>
        <source>Sensors</source>
        <translation>Sensors</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="123"/>
        <source>Disabled sensors won&apos;t be updated from hardware</source>
        <translation>Els sensors deshabilitats no s&apos;actualizarán des del maquinari</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="133"/>
        <source>Device</source>
        <translation>Dispositiu</translation>
    </message>
</context>
<context>
    <name>SysTray</name>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Hide</source>
        <translation>Amagar</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="164"/>
        <source>Manual profiles</source>
        <translation>Perfils manuals</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="168"/>
        <source>Quit</source>
        <translation>Sortir</translation>
    </message>
</context>
<context>
    <name>System</name>
    <message>
        <location filename="../qml/System.qml" line="88"/>
        <source>Information and application settings...</source>
        <translation>Informació i paràmetres de la aplicació...</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="98"/>
        <source>Settings</source>
        <translation>Paràmetres</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="104"/>
        <source>Copy all</source>
        <translation>Copiar tot</translation>
    </message>
</context>
<context>
    <name>SystemInfoUI</name>
    <message>
        <location filename="../core/systeminfoui.cpp" line="19"/>
        <source>kernelv</source>
        <translation>Versió del nucli</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="20"/>
        <source>mesav</source>
        <translation>Versió de Mesa</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="21"/>
        <source>vkapiv</source>
        <translation>Versió de la API de Vulkan</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="22"/>
        <source>glcorev</source>
        <translation>Versió de OpenGL (core)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="23"/>
        <source>glcompv</source>
        <translation>Versió de OpenGL (compat)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="24"/>
        <source>vendorid</source>
        <translation>ID del proveïdor</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="25"/>
        <source>deviceid</source>
        <translation>ID del dispositiu</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="26"/>
        <source>svendorid</source>
        <translation>ID del model del proveïdor</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="27"/>
        <source>sdeviceid</source>
        <translation>ID del model</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="28"/>
        <source>vendor</source>
        <translation>Proveïdor</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="29"/>
        <source>device</source>
        <translation>Dispositiu</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="30"/>
        <source>sdevice</source>
        <translation>Model de dispositiu</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="31"/>
        <source>pcislot</source>
        <translation>Slot PCI</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="32"/>
        <source>driver</source>
        <translation>Controlador</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="33"/>
        <source>revision</source>
        <translation>Revisió</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="34"/>
        <source>memory</source>
        <translation>Memòria</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="35"/>
        <source>gputype</source>
        <translation>Tipus de GPU</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="36"/>
        <source>biosv</source>
        <translation>Versió de BIOS</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="37"/>
        <source>cpufamily</source>
        <translation>Familia de CPU</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="38"/>
        <source>model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="39"/>
        <source>modname</source>
        <translation>Nom del model</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="40"/>
        <source>stepping</source>
        <translation>Revisió</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="41"/>
        <source>ucodev</source>
        <translation>Versió del microcódi</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="42"/>
        <source>l3cache</source>
        <translation>Caché L3</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="44"/>
        <source>cores</source>
        <translation>Nuclis</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="45"/>
        <source>flags</source>
        <translation>Indicadors</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="46"/>
        <source>bugs</source>
        <translation>Bugs</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="47"/>
        <source>bogomips</source>
        <translation>Bogomips</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="48"/>
        <source>arch</source>
        <translation>Arquitectura</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="49"/>
        <source>opmode</source>
        <translation>Mode d&apos;operació</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="50"/>
        <source>byteorder</source>
        <translation>Ordre de bytes</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="51"/>
        <source>virt</source>
        <translation>Virtualizació</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="52"/>
        <source>l1dcache</source>
        <translation>Caché L1 (dades)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="53"/>
        <source>l1icache</source>
        <translation>Caché L1 (instruccions)</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="54"/>
        <source>l2cache</source>
        <translation>Caché L2</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="55"/>
        <source>uniqueid</source>
        <translation>ID únic</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="43"/>
        <source>exeunits</source>
        <translation>Unitats d&apos;execució</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="43"/>
        <source>Profiles</source>
        <translation>Perfils</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="46"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
</context>
</TS>
