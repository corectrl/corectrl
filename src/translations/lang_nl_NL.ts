<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>AMD::PMFixedQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="17"/>
        <source>low</source>
        <translation>Laag</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="18"/>
        <source>mid</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="19"/>
        <source>high</source>
        <translation>Hoog</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqRangeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="20"/>
        <source>SCLK</source>
        <translation>SCLK</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqrange/pmfreqrangeqmlitem.cpp" line="21"/>
        <source>MCLK</source>
        <translation>MCLK</translation>
    </message>
</context>
<context>
    <name>AMD::PMFreqVoltQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="18"/>
        <source>SCLK</source>
        <translation>SCLK</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/overdrive/freqvolt/pmfreqvoltqmlitem.cpp" line="19"/>
        <source>MCLK</source>
        <translation>MCLK</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerProfileQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="16"/>
        <source>3D_FULL_SCREEN</source>
        <translation>3D + Schermvullend</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="17"/>
        <source>POWER_SAVING</source>
        <translation>Energiebesparing</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="18"/>
        <source>VIDEO</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="19"/>
        <source>VR</source>
        <translation>VR</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/powerprofile/pmpowerprofileqmlitem.cpp" line="20"/>
        <source>COMPUTE</source>
        <translation>Berekeningen</translation>
    </message>
</context>
<context>
    <name>AMD::PMPowerStateQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="17"/>
        <source>battery</source>
        <translation>Accu</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="18"/>
        <source>balanced</source>
        <translation>Balans</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="19"/>
        <source>performance</source>
        <translation>Prestaties</translation>
    </message>
</context>
<context>
    <name>AMDFanCurveForm</name>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="42"/>
        <source>Temperature</source>
        <translation>Temperatuur</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="43"/>
        <source>PWM</source>
        <translation>PWM</translation>
    </message>
    <message>
        <location filename="../qml/AMDFanCurveForm.qml" line="119"/>
        <source>Fan start</source>
        <translation>Ventilator begint bij</translation>
    </message>
</context>
<context>
    <name>AMDFanFixedForm</name>
    <message>
        <location filename="../qml/AMDFanFixedForm.qml" line="82"/>
        <source>Fan start</source>
        <translation>Ventilator begint bij</translation>
    </message>
</context>
<context>
    <name>AMDFanModeForm</name>
    <message>
        <location filename="../qml/AMDFanModeForm.qml" line="19"/>
        <source>Ventilation</source>
        <translation>Ventilatie</translation>
    </message>
</context>
<context>
    <name>AMDOdFanCurveForm</name>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="24"/>
        <source>Temperature</source>
        <translation>Temperatuur</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="25"/>
        <source>Speed</source>
        <translation>Snelheid</translation>
    </message>
    <message>
        <location filename="../qml/AMDOdFanCurveForm.qml" line="116"/>
        <source>Fan stop</source>
        <translation>Ventilator stopt</translation>
    </message>
</context>
<context>
    <name>AMDPMFixedFreqForm</name>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="75"/>
        <source>GPU</source>
        <translation>Gpu</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFixedFreqForm.qml" line="105"/>
        <source>Memory</source>
        <translation>Geheugen</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqModeForm</name>
    <message>
        <location filename="../qml/AMDPMFreqModeForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>Frequentie</translation>
    </message>
</context>
<context>
    <name>AMDPMFreqOdForm</name>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="46"/>
        <source>GPU</source>
        <translation>Gpu</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMFreqOdForm.qml" line="77"/>
        <source>Memory</source>
        <translation>Geheugen</translation>
    </message>
</context>
<context>
    <name>AMDPMPerfModeForm</name>
    <message>
        <location filename="../qml/AMDPMPerfModeForm.qml" line="19"/>
        <source>Performance mode</source>
        <translation>Prestatiemodus</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerCapForm</name>
    <message>
        <location filename="../qml/AMDPMPowerCapForm.qml" line="37"/>
        <source>Power limit</source>
        <translation>Energiebeperking</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerProfileForm</name>
    <message>
        <location filename="../qml/AMDPMPowerProfileForm.qml" line="56"/>
        <source>Power profile</source>
        <translation>Energieprofiel</translation>
    </message>
</context>
<context>
    <name>AMDPMPowerStateModeForm</name>
    <message>
        <location filename="../qml/AMDPMPowerStateModeForm.qml" line="19"/>
        <source>Power management mode</source>
        <translation>Energiebeheermodus</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltCurveForm</name>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="20"/>
        <source>Frequency</source>
        <translation>Frequentie</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="21"/>
        <location filename="../qml/AMDPMVoltCurveForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Voltage</translation>
    </message>
</context>
<context>
    <name>AMDPMVoltOffsetForm</name>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="51"/>
        <source>Voltage</source>
        <translation>Voltage</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="64"/>
        <source>WARNING: Operating range not available. Use with caution!</source>
        <translation>WAARSCHUWING: het gekozen bereik is niet beschikbaar. Wees voorzichtig!</translation>
    </message>
    <message>
        <location filename="../qml/AMDPMVoltOffsetForm.qml" line="93"/>
        <source>OFFSET</source>
        <translation>Verschuiving</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="40"/>
        <source>Control your hardware with ease using application profiles</source>
        <translation>Bedien uw hardware op basis van programmaprofielen</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="45"/>
        <source>by</source>
        <translation>door</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="54"/>
        <source>Links</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="55"/>
        <source>Project</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="56"/>
        <source>Issue tracker</source>
        <translation>Probleemtracker</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="57"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="58"/>
        <source>FAQ</source>
        <translation>Veelgestelde vragen</translation>
    </message>
</context>
<context>
    <name>CPUFreqForm</name>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="93"/>
        <source>Frequency governor</source>
        <translation>Frequentiegovernor</translation>
    </message>
    <message>
        <location filename="../qml/CPUFreqForm.qml" line="119"/>
        <source>Energy Performance Preference</source>
        <translation>Energie-efficiëntie voorkeur</translation>
    </message>
</context>
<context>
    <name>CPUFreqModeForm</name>
    <message>
        <location filename="../qml/CPUFreqModeForm.qml" line="19"/>
        <source>Performance scaling</source>
        <translation>Prestatieverhoudingen</translation>
    </message>
</context>
<context>
    <name>CPUFreqQMLItem</name>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="17"/>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="25"/>
        <source>performance</source>
        <translation>performance</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="18"/>
        <source>powersave</source>
        <translation>powersave</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="19"/>
        <source>schedutil</source>
        <translation>Schedutil</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="20"/>
        <source>ondemand</source>
        <translation>ondemand</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="21"/>
        <source>conservative</source>
        <translation>conservative</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="24"/>
        <source>default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="26"/>
        <source>balance_performance</source>
        <translation>Prestaties balanceren</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="27"/>
        <source>balance_power</source>
        <translation>Energie balanceren</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="28"/>
        <source>power</source>
        <translation>Energie</translation>
    </message>
</context>
<context>
    <name>ControlModeQMLItem</name>
    <message>
        <location filename="../core/components/controls/amd/fan/auto/fanautoqmlitem.cpp" line="16"/>
        <source>AMD_FAN_AUTO</source>
        <translation>Automatische ventilatie</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/curve/fancurveqmlitem.cpp" line="19"/>
        <source>AMD_FAN_CURVE</source>
        <translation>Ventilatiecurve</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/fixed/fanfixedqmlitem.cpp" line="13"/>
        <source>AMD_FAN_FIXED</source>
        <translation>Vaste ventilatorsnelheid</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/dynamicfreq/pmdynamicfreqqmlitem.cpp" line="16"/>
        <source>AMD_PM_DYNAMIC_FREQ</source>
        <translation>Dynamische frequentie</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/fixedfreq/pmfixedfreqqmlitem.cpp" line="17"/>
        <source>AMD_PM_FIXED_FREQ</source>
        <translation>Vaste frequentie</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/advanced/pmadvancedqmlitem.cpp" line="16"/>
        <source>AMD_PM_ADVANCED</source>
        <translation>Geavanceerd</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/auto/pmautoqmlitem.cpp" line="16"/>
        <source>AMD_PM_AUTO</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/fixed/pmfixedqmlitem.cpp" line="16"/>
        <source>AMD_PM_FIXED</source>
        <translation>Vast</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/pm/powerstate/pmpowerstateqmlitem.cpp" line="16"/>
        <source>AMD_PM_POWERSTATE</source>
        <translation>Energiestatus</translation>
    </message>
    <message>
        <location filename="../core/components/controls/cpu/cpufreqqmlitem.cpp" line="14"/>
        <source>CPU_CPUFREQ</source>
        <translation>Cpu-frequentie</translation>
    </message>
    <message>
        <location filename="../core/components/controls/noopqmlitem.cpp" line="16"/>
        <source>NOOP</source>
        <translation>Noop</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/auto/odfanautoqmlitem.cpp" line="16"/>
        <source>AMD_OD_FAN_AUTO</source>
        <translation>Automatische ventilatie</translation>
    </message>
    <message>
        <location filename="../core/components/controls/amd/fan/overdrive/curve/odfancurveqmlitem.cpp" line="18"/>
        <source>AMD_OD_FAN_CURVE</source>
        <translation>Ventilatiecurve</translation>
    </message>
</context>
<context>
    <name>FVControl</name>
    <message>
        <location filename="../qml/FVControl.qml" line="105"/>
        <source>STATE</source>
        <translation>STATUS</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="168"/>
        <source>Auto</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="201"/>
        <source>Frequency</source>
        <translation>Frequentie</translation>
    </message>
    <message>
        <location filename="../qml/FVControl.qml" line="225"/>
        <source>Voltage</source>
        <translation>Voltage</translation>
    </message>
</context>
<context>
    <name>FreqStateControl</name>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="61"/>
        <source>MINIMUM</source>
        <translation>MINIMUM</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="63"/>
        <source>MAXIMUM</source>
        <translation>MAXIMUM</translation>
    </message>
    <message>
        <location filename="../qml/FreqStateControl.qml" line="65"/>
        <source>STATE</source>
        <translation>STATUS</translation>
    </message>
</context>
<context>
    <name>NoopForm</name>
    <message>
        <location filename="../qml/NoopForm.qml" line="41"/>
        <source>Warning!</source>
        <translation>Waarschuwing!</translation>
    </message>
    <message>
        <location filename="../qml/NoopForm.qml" line="46"/>
        <source>The component will not be controlled</source>
        <translation>Dit onderdeel wordt niet bediend</translation>
    </message>
</context>
<context>
    <name>ProfileButton</name>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Disable</source>
        <translation>Uitschakelen</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="146"/>
        <source>Enable</source>
        <translation>Inschakelen</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="154"/>
        <source>Edit...</source>
        <translation>Bewerken…</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="162"/>
        <source>Clone...</source>
        <translation>Klonen…</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="169"/>
        <source>Export to...</source>
        <translation>Exporteren naar…</translation>
    </message>
    <message>
        <location filename="../qml/ProfileButton.qml" line="178"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
</context>
<context>
    <name>ProfileInfoDialog</name>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="128"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="137"/>
        <source>Profile name</source>
        <translation>Profielnaam</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="143"/>
        <source>Activation:</source>
        <translation>Activering:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="154"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="155"/>
        <source>Manual</source>
        <translation>Handmatig</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="166"/>
        <source>Executable:</source>
        <translation>Uitvoerbaar bestand:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="178"/>
        <source>Executable name</source>
        <translation>Uitvoerbaar bestand</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="195"/>
        <source>Select an executable file</source>
        <translation>Kies een uitvoerbaar bestand</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="208"/>
        <source>Icon:</source>
        <translation>Pictogram:</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="238"/>
        <source>Select an icon</source>
        <translation>Kies een pictogram</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="239"/>
        <source>Images</source>
        <translation>Afbeeldingen</translation>
    </message>
    <message>
        <location filename="../qml/ProfileInfoDialog.qml" line="256"/>
        <source>Customize icon</source>
        <translation>Pictogram aanpassen</translation>
    </message>
</context>
<context>
    <name>ProfileManagerUI</name>
    <message>
        <location filename="../core/profilemanagerui.cpp" line="18"/>
        <source>_global_</source>
        <translation>Algemeen</translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <location filename="../qml/Profiles.qml" line="150"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="241"/>
        <source>Profile properties</source>
        <translation>Profieleigenschappen</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="255"/>
        <location filename="../qml/Profiles.qml" line="306"/>
        <source>New profile properties</source>
        <translation>Nieuwe profieleigenschappen</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="286"/>
        <source>Export profile to...</source>
        <translation>Profiel exporteren naar…</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="287"/>
        <location filename="../qml/Profiles.qml" line="565"/>
        <source>CoreCtrl profile</source>
        <translation>CoreCtrl-profiel</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="295"/>
        <source>Cannot export profile.
Check the permissions of the destination file and directory.</source>
        <translation>Het profiel kan niet worden geëxporteerd.
Controleer de rechten van de bestemming.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="336"/>
        <source>Error</source>
        <translation>Foutmelding</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="357"/>
        <location filename="../qml/Profiles.qml" line="387"/>
        <location filename="../qml/Profiles.qml" line="422"/>
        <location filename="../qml/Profiles.qml" line="451"/>
        <source>Warning</source>
        <translation>Waarschuwing</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="372"/>
        <source>This action is permantent.
Do you really want to remove %1?</source>
        <translation>Deze actie kan niet ongedaan worden gemaakt.
Weet u zeker dat u %1 wilt verwijderen?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="399"/>
        <source>Unapplied settings will be lost.
Do you want to apply them now?</source>
        <translation>Niet-toegepaste instellingen worden gewist.
Wilt u ze nu toepassen?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="434"/>
        <source>Unsaved settings will be lost.
</source>
        <translation>Niet-toegepaste instellingen worden gewist.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="435"/>
        <source>Do you want to load the default settings?</source>
        <translation>Wilt u de standaardwaarden herstellen?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="463"/>
        <source>Current settings will be discarded.
</source>
        <translation>De huidige instellingen worden hierdoor overschreven.
</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="464"/>
        <source>Do you want to load the saved settings?</source>
        <translation>Wilt u de opgeslagen instellingen laden?</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="486"/>
        <source>Manage profiles for your applications...</source>
        <translation>Beheer uw programmaprofielen…</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="535"/>
        <source>Save</source>
        <translation>Opslaan</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="541"/>
        <source>Apply</source>
        <translation>Toepassen</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="547"/>
        <source>Restore</source>
        <translation>Herstellen</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="559"/>
        <source>Load from...</source>
        <translation>Laden uit…</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="564"/>
        <source>Load settings from...</source>
        <translation>Instellingen laden uit…</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="570"/>
        <source>Cannot load profile.
Invalid or corrupted file.</source>
        <translation>Het profiel kan niet worden geladen.
Het bestand is mogelijk ongeldig of beschadigd.</translation>
    </message>
    <message>
        <location filename="../qml/Profiles.qml" line="579"/>
        <source>Reset</source>
        <translation>Standaardwaarden</translation>
    </message>
</context>
<context>
    <name>SensorGraph</name>
    <message>
        <location filename="../qml/SensorGraph.qml" line="136"/>
        <source>n/a</source>
        <translation>n/b</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/activitygraphitem.cpp" line="19"/>
        <source>AMD_ACTIVITY</source>
        <translation>Activiteit</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedpercgraphitem.cpp" line="19"/>
        <source>AMD_FAN_SPEED_PERC</source>
        <translation>Ventilatorsnelheid (in procenten)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/fanspeedrpmgraphitem.cpp" line="21"/>
        <source>AMD_FAN_SPEED_RPM</source>
        <translation>Ventilatorsnelheid (in TPM)</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpufreqgraphitem.cpp" line="19"/>
        <source>AMD_GPU_FREQ</source>
        <translation>Gpu-frequentie</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gputempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_TEMP</source>
        <translation>Gpu-temperatuur</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/gpuvoltgraphitem.cpp" line="19"/>
        <source>AMD_GPU_VOLT</source>
        <translation>Gpu-voltage</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/junctiontempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_JUNCTION_TEMP</source>
        <translation>Gpu-scheidingstemperatuur</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memfreqgraphitem.cpp" line="19"/>
        <source>AMD_MEM_FREQ</source>
        <translation>Geheugenfrequentie</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memorytempgraphitem.cpp" line="19"/>
        <source>AMD_GPU_MEMORY_TEMP</source>
        <translation>Tijdelijk gpu-geheugen</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/memusagegraphitem.cpp" line="19"/>
        <source>AMD_MEM_USAGE</source>
        <translation>Geheugengebruik</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/amd/powergraphitem.cpp" line="19"/>
        <source>AMD_POWER</source>
        <translation>Energie</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpufreqpackgraphitem.cpp" line="19"/>
        <source>CPU_FREQ_PACK</source>
        <translation>Frequentiereeks</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpuusagegraphitem.cpp" line="19"/>
        <source>CPU_USAGE</source>
        <translation>Gebruik</translation>
    </message>
    <message>
        <location filename="../core/components/sensors/cpu/cpucoretempgraphitem.cpp" line="19"/>
        <source>CPU_CORE_TEMP</source>
        <translation>Temperatuur</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="12"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>General</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsDialog.qml" line="32"/>
        <source>Workarounds</source>
        <translation>Tussenoplossingen</translation>
    </message>
</context>
<context>
    <name>SettingsGeneral</name>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="64"/>
        <source>Show system tray icon</source>
        <translation>Systeemvakpictogram tonen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="79"/>
        <source>Start minimized on system tray</source>
        <translation>Geminimaliseerd opstarten</translation>
    </message>
    <message>
        <location filename="../qml/SettingsGeneral.qml" line="93"/>
        <source>Save window geometry</source>
        <translation>Venstergeometrie opslaan</translation>
    </message>
</context>
<context>
    <name>SettingsWorkarounds</name>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="112"/>
        <source>Sensors</source>
        <translation>Sensoren</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="123"/>
        <source>Disabled sensors won&apos;t be updated from hardware</source>
        <translation>Uitgeschakelde sensoren worden niet bijgewerkt door de hardware</translation>
    </message>
    <message>
        <location filename="../qml/SettingsWorkarounds.qml" line="133"/>
        <source>Device</source>
        <translation>Apparaat</translation>
    </message>
</context>
<context>
    <name>SysTray</name>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Hide</source>
        <translation>Verbergen</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="121"/>
        <source>Show</source>
        <translation>Tonen</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="164"/>
        <source>Manual profiles</source>
        <translation>Handmatige profielen</translation>
    </message>
    <message>
        <location filename="../app/systray.cpp" line="168"/>
        <source>Quit</source>
        <translation>Afsluiten</translation>
    </message>
</context>
<context>
    <name>System</name>
    <message>
        <location filename="../qml/System.qml" line="88"/>
        <source>Information and application settings...</source>
        <translation>Informatie en programma-instellingen…</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="98"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../qml/System.qml" line="104"/>
        <source>Copy all</source>
        <translation>Alles kopiëren</translation>
    </message>
</context>
<context>
    <name>SystemInfoUI</name>
    <message>
        <location filename="../core/systeminfoui.cpp" line="19"/>
        <source>kernelv</source>
        <translation>kernelv.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="20"/>
        <source>mesav</source>
        <translation>mesav.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="21"/>
        <source>vkapiv</source>
        <translation>vkapiv.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="22"/>
        <source>glcorev</source>
        <translation>glcorev.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="23"/>
        <source>glcompv</source>
        <translation>glcompv.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="24"/>
        <source>vendorid</source>
        <translation>fabrikantid</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="25"/>
        <source>deviceid</source>
        <translation>apparaatid</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="26"/>
        <source>svendorid</source>
        <translation>sapparaatid</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="27"/>
        <source>sdeviceid</source>
        <translation>sapparaatid</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="28"/>
        <source>vendor</source>
        <translation>fabrikant</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="29"/>
        <source>device</source>
        <translation>apparaat</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="30"/>
        <source>sdevice</source>
        <translation>sapparaat</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="31"/>
        <source>pcislot</source>
        <translation>pcislot</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="32"/>
        <source>driver</source>
        <translation>stuurprogramma</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="33"/>
        <source>revision</source>
        <translation>revisie</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="34"/>
        <source>memory</source>
        <translation>geheugen</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="35"/>
        <source>gputype</source>
        <translation>gputype</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="36"/>
        <source>biosv</source>
        <translation>biosv.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="37"/>
        <source>cpufamily</source>
        <translation>cpufamilie</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="38"/>
        <source>model</source>
        <translation>model</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="39"/>
        <source>modname</source>
        <translation>modelnaam</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="40"/>
        <source>stepping</source>
        <translation>stappen</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="41"/>
        <source>ucodev</source>
        <translation>ucodev.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="42"/>
        <source>l3cache</source>
        <translation>l3cache</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="43"/>
        <source>exeunits</source>
        <translation>uitvoereenheden</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="44"/>
        <source>cores</source>
        <translation>kernen</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="45"/>
        <source>flags</source>
        <translation>opties</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="46"/>
        <source>bugs</source>
        <translation>bugs</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="47"/>
        <source>bogomips</source>
        <translation>bogomips</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="48"/>
        <source>arch</source>
        <translation>arch.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="49"/>
        <source>opmode</source>
        <translation>opmodus</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="50"/>
        <source>byteorder</source>
        <translation>bytevolgorde</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="51"/>
        <source>virt</source>
        <translation>virt.</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="52"/>
        <source>l1dcache</source>
        <translation>l1dcache</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="53"/>
        <source>l1icache</source>
        <translation>l1icache</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="54"/>
        <source>l2cache</source>
        <translation>l2cache</translation>
    </message>
    <message>
        <location filename="../core/systeminfoui.cpp" line="55"/>
        <source>uniqueid</source>
        <translation>Unieke ID</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="43"/>
        <source>Profiles</source>
        <translation>Profielen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="46"/>
        <source>System</source>
        <translation>Systeem</translation>
    </message>
</context>
</TS>
