// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2025 Juan Palacios <jpalaciosdev@gmail.com>

import QtQuick 2.15
import QtQuick.Controls 2.15
import "Style.js" as Style

Pane {
  padding: Style.g_padding

  background: Rectangle {
    anchors.fill: parent
    color: "transparent"
  }
}
