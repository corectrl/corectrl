Please, choose a template for your issue using the **template selector** under the *Description* field.

All driver issues must be [reported upstream](https://gitlab.com/corectrl/corectrl/-/wikis/FAQ#i-found-a-driver-bug-where-can-i-report-it). You can easily check if the issue is a driver bug by trying to reproduce it using different kernel versions. When an issue is only reproducible on some kernels versions, it's most likely due to a driver bug.

**Do not report driver issues in this issue tracker**. Exceptionally, you can report **severe driver issues** here if you feel that other CoreCtrl users must be aware about them. Please, label these issues using the `driver issue` tag.

Any issues not following the previous recommendations and the [Issues writing guidelines](CONTRIBUTING.md#Issues-writing-guidelines) **may be closed as invalid** without further explanation.
